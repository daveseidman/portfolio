# Portfolio

Fullstack XR Developer.

### TODO:

- Draco not available offline
- diagnose horizontal resize issue with carousel (bouncing);
- turn holochat back on?
- device size, landscape, make font smaller
- remove unused glyphs to shrink font filesize
- Match slug case insensitive
- fix hover states for mobile (toggle .active instead)
- add preloader
- add chimes interaction sounds
- cursor movement, interact with shape more?
- browser back / forward buttons create incorrect history states
- Lazy Load images / videos, currently they begin downloading on app load
- update linkedin & twitter
- Add ASCO, CES, DLS, Spotify Codes, Motion Graphics
- still able to open projects while scrolling away from them (mostly touch)
- test video playback on slower devices, consider turning down pixel ratio while projects are open
- investigate performance
- move savetab somewhere else and turn off ec2?
